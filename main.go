package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"time"
)

func countFile(path string, result chan<- int) {
	file, _ := os.Open(path)
	defer file.Close()

	count := 0
	fileScanner := bufio.NewScanner(file)
	for fileScanner.Scan() {
		count++
	}
	result <- count
}

func countDirectory(path string, matcher *regexp.Regexp, out chan<- int) {
	files, err := ioutil.ReadDir(path)
	if err != nil {
		panic(err)
	}

	results := make(chan int, len(files))
	defer close(results)

	mathes := 0

	for _, file := range files {
		if file.IsDir() {
			go countDirectory(fmt.Sprintf("%s/%s", path, file.Name()), matcher, results)
			mathes++
		} else if matcher.MatchString(file.Name()) {
			go countFile(fmt.Sprintf("%s/%s", path, file.Name()), results)
			mathes++
		}
	}

	lineCount := 0

	for i := 0; i < mathes; i++ {
		lineCount += <-results
	}

	out <- lineCount
}

func CountFilesThatMatch(path string, matcher *regexp.Regexp) int {
	results := make(chan int)
	defer close(results)
	go countDirectory("./", matcher, results)

	return <-results
}

func main() {
	regMatcherPtr := flag.String("match", "", "what you want to match")
	flag.Parse()
	start := time.Now()
	reg, err := regexp.Compile(*regMatcherPtr)
	if err != nil {
		panic(err)
	}
	log.Printf("Total Number of lines: %d", CountFilesThatMatch("./", reg))
	log.Printf("Execution took: %s", time.Since(start))
}
