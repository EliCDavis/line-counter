package main

import (
	"regexp"
	"testing"
)

func TestReadsFilesFromDirectory(t *testing.T) {
	reg, _ := regexp.Compile(".*\\.golden$")
	linesCounted := CountFilesThatMatch("./testdata", reg)
	if linesCounted != 10 {
		t.Errorf("Counted wrong number of lines! Got: %d", linesCounted)
	}
}

func BenchmarkReadsFile(b *testing.B) {
	reg, _ := regexp.Compile(".*\\.golden$")
	for n := 0; n < b.N; n++ {
		CountFilesThatMatch("./testdata", reg)
	}
}

// func BenchmarkBabelCodebase(b *testing.B) {
// 	reg, _ := regexp.Compile(".*\\.cs$")
// 	CountFilesThatMatch("C:\\dev\\projects\\babel\\Codebase", reg)
// }
